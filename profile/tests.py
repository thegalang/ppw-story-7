from django.test import TestCase, Client
from django.http import HttpRequest
from pyvirtualdisplay import Display
from . import views
from selenium import webdriver
import time
from datetime import datetime
from selenium.webdriver.chrome.options import Options
from selenium.webdriver import DesiredCapabilities
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.color import Color
class ProfileUnitTest(TestCase):

	def test_exist_landing(self):
		#print("masuk")
		response = Client().get('')
		self.assertEqual(response.status_code, 200)
 
	def test_exist_greeeting(self):

		response = Client().get('')

		self.assertContains(response, "Halo, selamat datang!", count=1, html=True)

	def test_exist_accordion_title(self):

		response = Client().get('')

		self.assertContains(response, "Aktivitasku", count=1, html=True)
		self.assertContains(response, "Organisasi dan Kepanitiaan", count=1, html=True)
		self.assertContains(response, "Prestasi", count=1, html=True)

class ProfileFunctionalTest(TestCase):

	# set-up chrome minimalis
	def setUp(self):
		# global display
		self.display = Display(visible=0, size=(800, 600))
		self.display.start()
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--disable-gpu')
		service_log_path='./chromedriver.log'
		service_args=['--verbose']
		self.browser = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)

	def tearDown(self):

		self.display.stop()
		self.browser.quit()

	# test apakah aktivitas bekerja
	def test_accordion_working(self):

		self.browser.get('localhost:8000')
		time.sleep(1)
		
		# pointer untuk setiap accordion
		acc_aktivitas = self.browser.find_element_by_id('1')
		acc_organisasi = self.browser.find_element_by_id('2')
		acc_prestasi = self.browser.find_element_by_id('3')

		# masih kosong
		self.assertNotIn("Menjadi orang baik :)", acc_aktivitas.text)
		self.assertNotIn("Staff BEM Keilmuan 2019", acc_organisasi.text)
		self.assertNotIn("Terlucu PMB 2018", acc_prestasi.text)

		# test klik aktivitas
		acc_aktivitas.click()
		time.sleep(3)
		self.assertIn("Menjadi orang baik :)", acc_aktivitas.text)
		self.assertNotIn("Staff BEM Keilmuan 2019", acc_organisasi.text)
		self.assertNotIn("Terlucu PMB 2018", acc_prestasi.text)
		
		# test klik organisasi
		acc_organisasi.click()
		time.sleep(3)
		self.assertNotIn("Menjadi orang baik :)", acc_aktivitas.text)
		self.assertIn("Staff BEM Keilmuan 2019", acc_organisasi.text)
		self.assertNotIn("Terlucu PMB 2018", acc_prestasi.text)

		# test klik prestasi
		acc_prestasi.click()
		time.sleep(3)
		self.assertNotIn("Menjadi orang baik :)", acc_aktivitas.text)
		self.assertNotIn("Staff BEM Keilmuan 2019", acc_organisasi.text)
		self.assertIn("Terlucu PMB 2018", acc_prestasi.text)

		# test tutup
		acc_prestasi.click()
		time.sleep(3)
		self.assertNotIn("Menjadi orang baik :)", acc_aktivitas.text)
		self.assertNotIn("Staff BEM Keilmuan 2019", acc_organisasi.text)
		self.assertNotIn("Terlucu PMB 2018", acc_prestasi.text)
		#self.assertIn("Coba coba", self.browser.page_source)
	
	def check_if_spotify_theme(self):
		
		body = self.browser.find_element_by_id("spotify")

		# test apakah warnanya sudah sesuai
		self.assertEqual(Color.from_string(body.value_of_css_property('background-color')).hex,
						'#2d283e')

		accor = self.browser.find_element_by_class_name("accordion")
		self.assertEqual(Color.from_string(accor.value_of_css_property('background-color')).hex,
						 '#caa9e6')
		self.assertEqual(Color.from_string(accor.value_of_css_property('border-color')).hex,
						 '#802bb1')

		self.assertEqual(Color.from_string(accor.value_of_css_property('color')).hex,
						 '#2d283e')
		time.sleep(3)
		

	def check_if_jurnalgalang_theme(self):
		
		body = self.browser.find_element_by_id("jurnalgalang")

		# test apakah warnanya sudah sesuai
		self.assertEqual(Color.from_string(body.value_of_css_property('background-color')).hex,
						'#ffffff')

		accor = self.browser.find_element_by_class_name("accordion")
		self.assertEqual(Color.from_string(accor.value_of_css_property('background-color')).hex,
						 '#ffffff')
		self.assertEqual(Color.from_string(accor.value_of_css_property('border-bottom-color')).hex,
						 '#000000')

		self.assertEqual(Color.from_string(accor.value_of_css_property('color')).hex,
						 '#000000')
		time.sleep(3)

	def test_change_theme_working(self):

		self.browser.get('localhost:8000')
		time.sleep(2)

		self.check_if_spotify_theme()

		change_theme = self.browser.find_element_by_id("theme_switch_button")
		change_theme.click()
		time.sleep(3)

		self.check_if_jurnalgalang_theme()

		change_theme.click()
		time.sleep(3)

		self.check_if_spotify_theme()

		


