$(document).ready(function(){

	var panels = $('.accordion .accordion-slide').hide()

	$(".accordion").click(function(){

		if($(this).find(".accordion-slide").is(":hidden")) {
			panels.slideUp();
			$(this).find(".accordion-slide").slideDown(); 
		}
		else {
			$(this).find(".accordion-slide").slideUp();
		}
	});

	$('#theme_switch_button').click(function() {

		if($('body').attr('id') == "spotify") {
			$('body').attr('id', "jurnalgalang");
		}
		else {
			$('body').attr('id', "spotify");
		}
	});	
});
